$(document).ready(function(){
  $("#btn-sub").click(function(){
    clients = document.getElementById("clients").value;
    polling = document.getElementById("polling").value;
    creationinterval = document.getElementById("creationinterval").value;
    sigmessage = document.getElementById("sigmessage").value;
    jsonsize = document.getElementById("jsonsize").value;


    const URL = "http://localhost:3000";
    const MAX_CLIENTS = clients; // Maximum number of clients
    const POLLING_PERCENTAGE = polling;
    const CLIENT_CREATION_INTERVAL_IN_MS = creationinterval;
    const EMIT_INTERVAL_IN_MS = sigmessage;
    const DUMMY_JSON = {"date":"","dummy":[]}; // JSON data that passed to server and recieved back
    const DUMMY_COUNT = jsonsize; // size of JSON
    const JSON_STR = {"Lorem":"ipsum","dolor":"sit","amet":"consectetur","adipiscing":"elit"};
    let clientCount = 0;
    let lastClientCount = 0;
    let lastReport = new Date().getTime();
    let packetsSinceLastReport = 0;
    let totalRecieved = 0;
    let socketsDisconnectedSinceLastReport = 0;
    let latencyCalc = 0;
    let latePacketCounter = 0;
    let sentPacketCounter = 0;
    let totalSentPackets = 0;
    let totalDisconnectedSockets = 0;
    let totalLatePackets = 0;
    let ioServerDisconnectError = 0;
    let ioClientDisconnectError = 0;
    let pingTimoutError = 0;
    let transportCloseError = 0;
    let transportError = 0;
    let successfulReconnections = 0;
    let reconnectionAttempts = 0;
    let reconnectionErrors = 0;
    let reconnectionFails = 0;
    let totalErrors = 0;
    let newConnectionsSinceLastReport = 0;
    let totalConnections = 0;

    /* console.log( `clients: ${clients}; polling: ${polling}; creationinterval: ${creationinterval}; sigmessage: ${sigmessage};  jsonsize: ${jsonsize};`
     * ); */

    // creating json
    const createJson = () => {
      for (let i = 0; i < DUMMY_COUNT; i++) {
        DUMMY_JSON["dummy"].push(JSON_STR);
      };
    };

    createJson();

    const createClient = () => {
      // for demonstration purposes, some clients stay stuck in HTTP long-polling
      const transports =
        Math.random() < POLLING_PERCENTAGE ? ["polling"] : ["polling", "websocket"];

      const socket = io(URL, {
        transports,
      });

      // client creation
      setInterval(() => {
        DUMMY_JSON["date"] = Date.now();
        socket.emit("client to server event", DUMMY_JSON);
        sentPacketCounter++;
      }, EMIT_INTERVAL_IN_MS);

      // server packet recieving
      socket.on("server to client event", (data) => {
        travelTime = Date.now() - data["date"];
        latencyCalc = latencyCalc + travelTime;
        if (travelTime > 500 ) {
          latePacketCounter++;
        };
        packetsSinceLastReport++;
      });

      socket.on("connect", () => {
        newConnectionsSinceLastReport++;
      });

      // disconnection detection
      socket.on("disconnect", (reason) => {
        /*         console.log(`disconnect due to ${reason}`); */
        $('#errorLog').append(reason);
        switch (reason) {
          case "io server disconnect":
            ioServerDisconnectError++;
            $('#ioServerDisconnectError').text("io server disconnect: " + ioServerDisconnectError);
            break;
          case "io client disconnect":
            ioClientDisconnectError++;
            $('#ioClientDisconnectError').text("io client disconnect: " + ioClientDisconnectError);
            break;
          case "ping timeout":
            pingTimoutError++;
            $('#pingTimoutError').text("ping timeout: " + pingTimoutError);
            break;
          case "transport close":
            transportCloseError++;
            $('#transportCloseError').text("transport close: " + transportCloseError);
            break;
          case "transport error":
            transportError++;
            $('#transportError').text("transport error: " + transportError);
            break;
        };
        socketsDisconnectedSinceLastReport++;
      });

      socket.io.on("error", (error) => {
        console.log(error);
        totalErrors++;
      });

      socket.io.on("reconnect", (attempt) => {
        successfulReconnections++;
      });

      socket.io.on("reconnect_attempt", (attempt) => {
        reconnectionAttempts++;
      });

      socket.io.on("reconnect_error", (error) => {
        reconnectionErrors++;
      });

      socket.io.on("reconnect_failed", () => {
        reconnectionFails++;
      });

      // checking max clients
      if (++clientCount < MAX_CLIENTS ) {
        setTimeout(createClient, CLIENT_CREATION_INTERVAL_IN_MS);
      }
    };

    createClient();

    const printReport = () => {
      const now = new Date().getTime();
      const durationSinceLastReport = (now - lastReport) / 1000;
      const newClientCount = clientCount - lastClientCount;
      totalConnections = totalConnections + newConnectionsSinceLastReport - socketsDisconnectedSinceLastReport;
      const socketsDisconnectedPerSecond = (
        packetsSinceLastReport / durationSinceLastReport
      ).toFixed(2);
      const packetsLostPerSecond = (
        socketsDisconnectedSinceLastReport / durationSinceLastReport
      ).toFixed(2);
      const averageLatency = (
        latencyCalc / packetsSinceLastReport
      ).toFixed(4);
      totalRecieved = totalRecieved + packetsSinceLastReport;
      const latePacketRatio = (
        latePacketCounter / packetsSinceLastReport
      ).toFixed(4);
      totalSentPackets = totalSentPackets + sentPacketCounter;
      totalDisconnectedSockets = totalDisconnectedSockets + socketsDisconnectedSinceLastReport;
      totalLatePackets = totalLatePackets + latePacketCounter;
      /* var outputText = `tot: ${clientCount}; new: ${newClientCount}; avg pkt per sec: ${socketsDisconnectedPerSecond}; avg pkt lost per sec: ${packetsLostPerSecond}; pkts: ${packetsSinceLastReport}; ltcy: ${averageLatency}; duration:${durationSinceLastReport}`
       * console.log(outputText); */
      //socketioLog.push(outputText);
      /* $('#outputString').text(outputText);
       * $('#outputFull').append(outputText + "\n"); */
      $('#clientCount').text("Total Client Count: "+ clientCount);
      $('#newClientCount').text("New Clients Since Last Report: "+ newClientCount);
      $('#totalRecieved').text("Total Packets Recieved: "+ totalRecieved);
      $('#socketsDisconnectedPerSecond').text("Packets Per Second: "+ socketsDisconnectedPerSecond);
      $('#packetsLostPerSecond').text("Disconnected Sockets Per Second: "+ packetsLostPerSecond);
      $('#packetsSinceLastReport').text("Packets Recieved Since Last Report: "+ packetsSinceLastReport);
      $('#socketsDisconnectedSinceLastReport').text("Disconnected Sockets Since Last Report: "+ socketsDisconnectedSinceLastReport);
      $('#averageLatency').text("Average Latency: "+ averageLatency);
      $('#durationSinceLastReport').text("Duration Since Last Report: "+ durationSinceLastReport);
      $('#latePacketCounter').text("Late Packets Since Last Report: "+ latePacketCounter);
      $('#latePacketRatio').text("Late to Recieved Ratio: "+ latePacketRatio);
      $('#sentPacketCounter').text("Sent Packets Since Last Report: "+ sentPacketCounter);
      $('#totalSentPackets').text("Total Sent Packets: "+ totalSentPackets);
      $('#totalDisconnectedSockets').text("Total Disconnected Sockets: "+ totalDisconnectedSockets);
      $('#totalLatePackets').text("Total Late Packets: "+ totalLatePackets);
      $('#totalErrors').text("Total Errors: "+ totalErrors);
      $('#successfulReconnections').text("Successful Reconnections: "+ successfulReconnections);
      $('#reconnectionAttempts').text("Reconnection Attempts: "+ reconnectionAttempts);
      $('#reconnectionErrors').text("Reconnection Errors: "+ reconnectionErrors);
      $('#reconnectionFails').text("Reconnect Failed: "+ reconnectionFails);
      $('#newConnectionsSinceLastReport').text("New Connections Since Last Report: "+ newConnectionsSinceLastReport);
      $('#totalConnections').text("Total Connections: "+ totalConnections);

      newConnectionsSinceLastReport = 0;
      sentPacketCounter = 0;
      lastClientCount = clientCount;
      latencyCalc = 0;
      packetsSinceLastReport = 0;
      socketsDisconnectedSinceLastReport = 0;
      lastReport = now;
      latePacketCounter = 0;
    };

    setInterval(printReport, 1000);

  });

});
