// setup basic express server
const express = require('express');
const app = express();
const path = require('path');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 3000;
let newPacketsRecieved = 0;
let totalPacketsRecieved = 0;
let totalSocketConnections = 0;
let newSocketConnections = 0;
let totalDisconnectedSockets = 0;
let newDisconnectedSockets = 0;
let socketServerDisconnectError = 0;
let socketClientDisconnectError = 0;
let serverShuttingDownError = 0;
let pingTimoutError = 0;
let transportCloseError = 0;
let transportError = 0;
let transportUnknownError = 0;
let sessionIDUnknownError = 0;
let badHandshakeMethodError = 0;
let badRequestError = 0;
let forbiddenError = 0;
let unsupportedProtocolVersionError = 0;
let newConnectionErrors = 0;
let totalConnectionErrors = 0;

server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// routing
app.use(express.static(path.join(__dirname, 'public')));

io.engine.on("connection_error", (err) => {
  newConnectionErrors++;

  switch (err.code) {
    case 0:
      transportUnknownError++;
      break;
    case 1:
      sessionIDUnknownError++;
      break;
    case 2:
      badHandshakeMethodError++;
      break;
    case 3:
      badRequestError++;
      break;
    case 4:
      forbiddenError++;
      break;
    case 5:
      unsupportedProtocolVersionError++;
      break;
  };
});
// chatroom
io.on('connection', function (socket) {
  newSocketConnections++;
  // accepting client's message
  socket.on('client to server event', (data) => {
    newPacketsRecieved++;

    // responding to client
    socket.emit('server to client event', data);
  });

  socket.on("disconnect", (reason) => {
    newDisconnectedSockets++;
    switch (reason) {
      case "socket server disconnect":
        socketServerDisconnectError++;
        break;
      case "socket client disconnect":
        socketClientDisconnectError++;
        break;
      case "server shutting down":
        serverShuttingDownError++;
        break;
      case "ping timeout":
        pingTimoutError++;
        break;
      case "transport close":
        transportCloseError++;
        break;
      case "transport error":
        transportError++;
        break;
    };
  });
});

const serverReport = () => {
  totalSocketConnections = totalSocketConnections + newSocketConnections - newDisconnectedSockets;
  totalPacketsRecieved = totalPacketsRecieved + newPacketsRecieved;
  totalDisconnectedSockets = totalDisconnectedSockets + newDisconnectedSockets;
  totalConnectionErrors = totalConnectionErrors + newConnectionErrors;
  const clientCount = io.engine.clientsCount;

  var outputString = `tscd:${clientCount}; tsc:${totalSocketConnections}; nsc:${newSocketConnections};|| tpr:${totalPacketsRecieved}; npr:${newPacketsRecieved};|| tds:${totalDisconnectedSockets}; nds:${newDisconnectedSockets};|| ssde:${socketServerDisconnectError}; scde:${socketClientDisconnectError}; ssde:${serverShuttingDownError}; pte:${pingTimoutError}; tce:${transportCloseError}; tee:${transportError};|| tce:${totalConnectionErrors}; nce:${newConnectionErrors};|| tue:${transportUnknownError}; siue:${sessionIDUnknownError}; bhme:${badHandshakeMethodError}; bre:${badRequestError}; fe:${forbiddenError}; upve:${unsupportedProtocolVersionError};`

  console.log(outputString);

  newSocketConnections = 0;
  newPacketsRecieved = 0;
  newDisconnectedSockets = 0;
  newConnectionErrors = 0;
};

setInterval(serverReport, 1000);
